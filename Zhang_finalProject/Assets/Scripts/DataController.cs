﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataController : MonoBehaviour
{
    static public DataController DC;
    public RoundData[] allRoundData;

    // Start is called before the first frame update
    void Awake()
    {
        if (DC == null)
        {
            DontDestroyOnLoad(gameObject);
            DC = this;
        }
        else
        {
            Destroy(gameObject);
        }

        //SceneManager.LoadScene("MenuScene");
    }

    public RoundData GetCurrentRoundData ()
    {
        return allRoundData[0];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
