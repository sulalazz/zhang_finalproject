﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CoffeeClass
{
    public string CoffeeName;
    public int CoffeeQuantity;
    public int CoffeeCost;
    public int CoffeeTotal;

    public CoffeeClass(string name, int cost)
    {
        CoffeeName = name;
        CoffeeCost = cost;
    }
}

public class UIManagerScript : MonoBehaviour
{
    public Animator menu;
    public Animator setting;
    public Animator paymentsuccess;
    public Animator shoppingcart;
    public Animator gamewarning;

    public Text purchaseQuantityText;
    public Text quantityText;
    public Text[] quantityTexts;
    public Button[] leftDecrease;
    public Button[] rightIncrease;

    public int max = 10;
    public int min = 0;

    int currentAmount = 0;
    int increasePerClick = 1;

    private int clickCount;
    

    public CoffeeClass[] coffeeData;
    public GameObject[] cofffeeLine;
    public Text[] coffeeTotal;

    void Awake()
    {
        coffeeData = new CoffeeClass[]
        {
            new CoffeeClass("Americano", 2),
            new CoffeeClass("Latte", 2),
            new CoffeeClass("Cappucino", 2),
            new CoffeeClass("Cold Brew", 2),
            new CoffeeClass("Mocha", 2),
            new CoffeeClass("Flat White", 2)
        };

    }

    public void OpenMenu()
    {
        menu.SetBool("isHidden", false);
    }

    public void CloseMenu()
    {
        menu.SetBool("isHidden", true);
    }

    public void OpenSetting()
    {
        setting.SetBool("isHidden", false);
    }

    public void CloseSetting()
    {
        setting.SetBool("isHidden", true);
    }

    public void OpenPayment()
    {
        paymentsuccess.SetBool("isHidden", false);
    }

    public void ClosePayment()
    {
        paymentsuccess.SetBool("isHidden", true);
    }

    public void OpenShoppingCart()
    {
        shoppingcart.SetBool("isHidden", false);
    }

    public void CloseShoppingCart()
    {
        shoppingcart.SetBool("isHidden", true);
    }

    public void StartGame()
    {
        if (clickCount >= 5)
        {
            SceneManager.LoadScene("GameScene");
            clickCount -= 5;
            PlayerPrefs.SetInt("clickCount", clickCount);
        }
        else
        {
            gamewarning.SetBool("isHidden", false);
        }
    }

    public void Start()
    {
        quantityText.text = currentAmount.ToString();
        purchaseQuantityText.text = "Number Purchased: " + ToString();

        clickCount = PlayerPrefs.GetInt("clickCount");
    }

    public void payClicked()
    {
        clickCount++;
        PlayerPrefs.SetInt("clickCount", clickCount);

        for (int i = 0; i < coffeeData.Length; i++)
        {
            coffeeData[i].CoffeeTotal = 0;
            cofffeeLine[i].SetActive(false);
            coffeeTotal[i].text = "";
        }
    }

    public void Update()
    {
        purchaseQuantityText.text = "Number Purchased: " + clickCount;
    }

    public void IncreaseValue(int coffeeIndex)
    {
        AdjustValue(true, coffeeIndex);
    }

    public void DecreaseValue(int coffeeIndex)
    {
        AdjustValue(false, coffeeIndex);
    }

    public void AdjustValue(bool increase, int coffeeIndex)
    {
        coffeeData[coffeeIndex].CoffeeQuantity = Mathf.Clamp(coffeeData[coffeeIndex].CoffeeQuantity + (increase ? increasePerClick : -increasePerClick), min, max);
        quantityTexts[coffeeIndex].text = coffeeData[coffeeIndex].CoffeeQuantity.ToString();

        leftDecrease[coffeeIndex].interactable = coffeeData[coffeeIndex].CoffeeQuantity > min;
        rightIncrease[coffeeIndex].interactable = coffeeData[coffeeIndex].CoffeeQuantity < max;
    }
    
    public void CloseGameWarning()
    {
        gamewarning.SetBool("isHidden", true);
    }

    public void AddtoCart(int coffeeIndex)
    {
        if (coffeeData[coffeeIndex].CoffeeQuantity == 0)
        {
            return;
        }

        coffeeData[coffeeIndex].CoffeeTotal += coffeeData[coffeeIndex].CoffeeQuantity;
        cofffeeLine[coffeeIndex].SetActive(true);
        coffeeTotal[coffeeIndex].text = coffeeData[coffeeIndex].CoffeeTotal.ToString();
    }

    public void SendEmail()
    {
        Application.OpenURL("https://mail.google.com/mail/u/0/?view=cm&fs=1&tf=1&source=mailto&to=ambzhang@iu.edu");
    }

}